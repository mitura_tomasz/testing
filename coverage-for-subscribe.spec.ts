import { fakeAsync, flush, TestBed, waitForAsync } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { AppComponent } from './app.component'
import { MyServiceService } from './my-service.service'

import { HttpClientTestingModule } from '@angular/common/http/testing'
import { of } from 'rxjs'
import { Injectable } from '@angular/core'

const todosMock = [
  {
    due_on: '2022-04-22T00:00:00.000+05:30',
    id: 1971,
    status: 'pending',
    title: 'Dens conculco debitis animadverto tener ustilo subnecto aro.',
    user_id: 4041,
  },
]

const usersMock = [
  {
    email: 'vaijayanti_mishra@bernier-dickens.org',
    gender: 'female',
    id: 3971,
    name: 'Vaijayanti Mishra',
    status: 'inactive',
  },
]

@Injectable()
class MockMyService extends MyServiceService {
  override getUsers() {
    return of(usersMock)
  }

  override getTodos() {
    return of(todosMock)
  }
}

describe('AppComponent', () => {
  let fixture: any
  let component: AppComponent

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      declarations: [AppComponent],
      providers: [
        {
          provide: MyServiceService,
          useClass: MockMyService,
        },
      ],
    }).compileComponents()

    fixture = TestBed.createComponent(AppComponent)
    component = fixture.componentInstance
  }))

  it('should get Todos', fakeAsync(() => {
    fixture.detectChanges()

    expect(component.todos.length).toBeGreaterThan(0)
    flush()
  }))
})
