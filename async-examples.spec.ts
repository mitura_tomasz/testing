import { fakeAsync, flush, flushMicrotasks, tick } from "@angular/core/testing"
import { of, pipe } from "rxjs"
import { delay } from "rxjs/operators"

describe("async test eamples", () => {
  it('Asynchronous test example with jasmine done()', (done: DoneFn) => {
    let test = false 

    setTimeout(()=>{
      test = true 

      expect(test).toBeTruthy()
      done()  
    }, 1000)
  })

  it('asynchronous tes example -set timeout', fakeAsync(() => {
    let test = false 

    setTimeout(()=>{})

    setTimeout(()=>{
      test = true 
    }, 1000) 

    tick(1000)
    flush()
    expect(test).toBeTruthy()
  }))

  it('asynchronous tes example - plain promise', fakeAsync(() => {
    let test = false 

    // task queue
    // setTimeout(()=>{ console.log('first setTimeout') })

    // micro-task queue
    Promise.resolve().then(() => {
      console.log('first Promise resolve')

      return Promise.resolve()
    }).then(()=> {
      console.log('second Promise resolve')
      test = true
    })

    flushMicrotasks()

    // is true because is expected after micro-task queue
    console.log('running the assertion')
    expect(test).toBeTruthy()

    // task queue is executed only after the micro-task queue is empty
    // browser runtime has two different types of queue for two different types of async operation
    // promises are lightweight and goes to micro-tasks
    // other like setTimeout goes to task-queue
  }))

  it('asynchronous tes example - plain promise + setTimeout()', fakeAsync(() => {
    let counter = 0;
    Promise.resolve().then(() => {
      counter += 10

      setTimeout(() => {
        counter += 1
      }, 1000)
    })
    
    expect(counter).toBe(0)
    flushMicrotasks()
    expect(counter).toBe(10)
    tick(500)
    expect(counter).toBe(10)
    tick(500)
    expect(counter).toBe(11)

  }))

  it('asynchronous test example - observables()', fakeAsync(() => {
    let test = false
    console.log('Creating Observable')
    const test$ = of(test).pipe(delay(1000))

    test$.subscribe(() => {
      test = true
    })

    tick(1000)

    console.log('running test assertions')
    expect(test).toBeTrue()
  }))


})